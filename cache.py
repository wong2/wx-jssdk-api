#-*-coding:utf-8-*-

from flask_caching import Cache
from config import CACHE_CONFIG

cache = Cache(config=CACHE_CONFIG)
