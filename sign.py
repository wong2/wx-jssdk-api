#-*-coding:utf-8-*-

import time
import hashlib
import random
import string
import logging
import requests
from cache import cache

logging.basicConfig(level=logging.DEBUG)

CACHE_EXPIRE_TIME = 3600


class GetAccessTokenError(Exception):
    pass

class GetTicketError(Exception):
    pass


class Signer(object):

    def __init__(self, app_id, app_secret):
        self.app_id = app_id
        self.app_secret = app_secret

    def __repr__(self):
        return '%s (%s)' % (self.__class__.__name__, self.app_id)

    def sign(self, url):
        ret = {
            'nonceStr': self._create_nonce_str(),
            'jsapi_ticket': self.get_ticket(),
            'timestamp': self._create_timestamp(),
            'url': url
        }
        s = '&'.join('%s=%s' % (key.lower(), value) for key, value in sorted(ret.items()))
        ret['signature'] = hashlib.sha1(s).hexdigest()
        return ret

    @cache.memoize(CACHE_EXPIRE_TIME)
    def get_ticket(self):
        url = 'http://arealme.wpweixin.com/api/get_weixin_js_api_ticket.json'
        r = requests.get(url, params={
            'access_token': 'isBz4Wux9bz6x9yHipDBFo9dsDdw3PUn',
        })
        resp = r.json()
        if resp.get('errcode', 0) != 0:
            raise GetTicketError(resp)

        return resp['ticket']

    @cache.memoize(CACHE_EXPIRE_TIME)
    def get_access_token(self):
        url = 'http://arealme.wpweixin.com/api/get_weixin_access_token.json'
        r = requests.get(url, params={
            'access_token': 'isBz4Wux9bz6x9yHipDBFo9dsDdw3PUn',
        })
        resp = r.json()
        if resp.get('errcode', 0) != 0:
            raise GetAccessTokenError(resp)

        return resp['access_token']

    def _create_nonce_str(self):
        return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(15))

    def _create_timestamp(self):
        return int(time.time())
